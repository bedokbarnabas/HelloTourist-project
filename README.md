During the project work, we created a web application in a JavaEE environment,
which helps volunteer, self-guided tour guides and foreign tourists find each other, thus promoting rural tourism.

We have created four authorization levels (admin, guest, traveler, guide) in the username and password protected interface.
We can browse as a guest without logging in.
As a registered user, we can book tours organized by registered guides.
Keeping in touch with tour guides is ensured via an online chat facility.
It is possible to schedule tours via the calendar system or by selecting the geographical location on the map.
Booked tours can be canceled at any time by both parties.
Travelers can evaluate tour guides subsequently for tours marked as complete and this evaluation will appear on the guides' profile too.

A projektmunka során egy olyan olyan webalkalmazást készítettünk JavaEE környezetben, 
ami segíti az önkéntes, önképző idegenvezetők és a külföldi turisták egymásra találását ezáltal is népszerűsítve a vidéki turizmust.
 
A felhasználónévvel és jelszóval védett felületen, négy jogosultsági szintet alakítottunk ki (admin, guest, traveler, guide). 
Bejelentkezés nélkül böngészhetünk „vendég”-ként,
Regisztrált felhasználóként lefoglalhatjuk, a regisztrált idegenvezetők által meghirdetett túrákat. 
A túravezetőkkel történő kapcsolattartás on-line chat felületen keresztül biztosított. 
A túrák befogalalása naptári rendszeren keresztül, de a földrajzi hely térképen történő kijelölésével is lehetséges.
A lefoglalt túrák bármikor mindkét fél által lemondhatók,. 
A túravezetőket, a befejezettnek jelölt túrák esetén, utólag értékelhetik az utazók, ami megjelenik a túravezetők profilján is.  